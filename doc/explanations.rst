Explanations
=============

The following are static descriptions of some parts of ewoksweb:

.. toctree::
    :maxdepth: 1

    explanations/editor_basics
    explanations/embedded_nodes
    explanations/link_validation
    explanations/node_editing
    explanations/link_editing
