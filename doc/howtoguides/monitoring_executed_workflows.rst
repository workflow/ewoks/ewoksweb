Monitoring executed workflows
=============================

The user can navigate to the monitoring page, where execution events are visualized for each
execution job, either by clicking on monitor button at the top left or by attempting to execute a
workflow. There under the title **Executed workflows** a number of executed and executing workflows
are presented. The difference between the two is that currently executing workflows will have a live
counter measuring the time elapsed from their submission. The fields that are presented are:

 - the name of the workflow in bold.
 - the time it was submitted for execution.
 - the time it took for its execution to be completed
 - the status which can be **Success** or **Failed**
 - the job-id on the right.
 - if it has a Failed status a complete error message is also prsented along with a descriptive traceback.

At the right of each entry an arrow button is accessible for enabling re-execution of the specific workflow.


