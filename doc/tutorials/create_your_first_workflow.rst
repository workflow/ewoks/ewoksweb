Create your first Workflow
==========================

By following the steps in this tutorial you will be able to create a basic workflow.

New Workflow
------------

When landing in ewoksweb you can start building your workflow immediately. The editor has in the middle
of the top bar the title **Untitled workflow (unsaved)** that will be replaced by the name
of the workflow once the workflow is first saved.

Add nodes
---------

On the left side the sidebar of tasks can be used to add nodes in your workflow. The tasks exist in
their categories that can be opened by clicking on them.

  1. Click on the **General** category that exists by default.

  2. Drag-and-Drop a **taskSkeleton** task in the canvas in the middle of the page twice.

  3. After dropping your tasks two nodes appear with the label taskSkeleton that can be re-arranged
     by dragging them on the canvas.


Add a link
-----------

Nodes can be connected with links that can be created in two ways:

  1. By clicking on the handles that the nodes have on their sides and sliding without
     releasing the click to a handle of another node.

  2. By clicking on a handle and then on another handle.



Save a Workflow
---------------

In order to save the workflow on the server:

  1. Click on the button with the disk at the right of the top bar.
  2. On the appearing dialog with the title **Give the new workflow identifier** give a name
     for your workflow.
  3. Click **SAVE WORKFLOW**
  4. If the name you provided is not unique an error message will appear at the bottom-left
     **Workflow 'theName' already exists**. In this case give another name and try again.

