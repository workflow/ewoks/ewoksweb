Executing a workflow
====================

In order to send a workflow for execution the user needs to open this workflow on canvas.
  1. using the quick open menu select and open the demo workflow

Any change can be made to the workflow before executing.
  2. Click on a node and adjust its default-inputs by adding or modifying one. For instance the
    delay input can be adjusted to change the time it will take for this task to be completed.
    Also a number can be given to both a and b default inputs that will be taken into account
    if there is no input from previous nodes in the workflow.
  3. Click on a link and change its data mapping. For instance the result of a source node can at the
    same time be mapped to a and/or b and/or delay inputs of the target node.

Time to execute our modified workflow.
  4. Click on the top-right menu on the navigation bar and select **Execute workflow**

In the dialog that will appear the user can provide some execution parameters.
  5.
