# ewoksweb

_ewoksweb_ is a web-based frontend for creating, visualizing and executing
[ewoks](https://ewoks.readthedocs.io/) workflows.

## Getting started

Install _ewoksweb_ python package locally

```bash
pip install ewoksweb
```

Start the server via

```bash
ewoksweb
```

## Documentation

https://ewoksweb.readthedocs.io/
