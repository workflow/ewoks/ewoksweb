const sidebarStyle = {
  formstyleflex: {
    display: 'flex',
    flexDirection: 'row' as const,
    flexWrap: 'wrap' as const,
    alignContent: 'flex-start',
    padding: '0px',
  },
  dropdown: {
    minWidth: '150px',
  },
  sectionHeader: {
    margin: '0.7rem 0 0 0',
  },
};

export default sidebarStyle;
