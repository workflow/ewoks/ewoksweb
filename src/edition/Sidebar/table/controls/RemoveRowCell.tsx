import { TableCell } from '@mui/material';

import RemoveRowButton from './RemoveRowButton';
import styles from './RemoveRowCell.module.css';

interface Props {
  disable?: boolean;
  onDelete: () => void;
}

function RemoveRowCell(props: Props) {
  const { disable, onDelete } = props;

  return (
    <TableCell className={styles.cell}>
      <RemoveRowButton disable={disable} onClick={onDelete} />
    </TableCell>
  );
}

export default RemoveRowCell;
