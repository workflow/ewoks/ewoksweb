import { TableCell } from '@mui/material';

import { RowType } from '../../../../types';
import styles from './TypeSelectCell.module.css';

interface Props {
  value: RowType;
  onChange: (newType: RowType) => void;
  disable?: boolean;
}

function TypeSelectCell(props: Props) {
  const { value, disable, onChange } = props;

  return (
    <TableCell className={styles.cell} align="left" size="small">
      <select
        className={styles.select}
        disabled={disable}
        value={value}
        onChange={(e) => onChange(e.target.value as RowType)}
        aria-label="Change input type"
      >
        {Object.values(RowType).map((type) => (
          <option key={type} value={type}>
            {type}
          </option>
        ))}
      </select>
    </TableCell>
  );
}

export default TypeSelectCell;
