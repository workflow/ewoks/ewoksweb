import InfoIcon from '@mui/icons-material/Info';
import { IconButton } from '@mui/material';

import type { NodeData } from '../../../types';
import sidebarStyle from '../sidebarStyle';
import SidebarTooltip from '../SidebarTooltip';
import TaskArrayProperty from './TaskArrayProperty';
import TaskIdentifier from './TaskIdentifier';
import TaskProperty from './TaskProperty';

interface Props {
  nodeId: string;
  nodeData: NodeData;
}

function NodeInfo(props: Props) {
  const { nodeId, nodeData } = props;
  const { task_props, ewoks_props } = nodeData;

  return (
    <>
      <h3 style={sidebarStyle.sectionHeader}>
        Task Info
        <SidebarTooltip
          text="These are properties of the task on which the node
      is based. They can only be changed by editing the relevant task except
      the task identifier which appoints another task to the node"
        >
          <IconButton size="small">
            <InfoIcon fontSize="small" />
          </IconButton>
        </SidebarTooltip>
      </h3>
      <TaskIdentifier nodeData={nodeData} nodeId={nodeId} />
      <TaskProperty label="Node Id" value={nodeId} />
      <TaskProperty label="Task Type" value={nodeData.task_props.task_type} />
      {ewoks_props.task_generator && (
        <TaskProperty label="Generator" value={ewoks_props.task_generator} />
      )}
      {task_props.category && (
        <TaskProperty label="Category" value={task_props.category} />
      )}
      <TaskArrayProperty
        label="Required Inputs"
        value={task_props.required_input_names}
        unknown={task_props.task_type !== 'class'}
      />
      <TaskArrayProperty
        label="Optional Inputs"
        value={task_props.optional_input_names}
        unknown={task_props.task_type !== 'class'}
      />

      <TaskArrayProperty
        label="Outputs"
        value={task_props.output_names}
        unknown={!['class', 'method', 'script'].includes(task_props.task_type)}
      />
    </>
  );
}

export default NodeInfo;
