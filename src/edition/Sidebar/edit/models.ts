import type { MarkerType } from '@xyflow/react';

export type MarkerEndOption = MarkerType | 'none';
