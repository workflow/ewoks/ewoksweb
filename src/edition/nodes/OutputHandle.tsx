import { Handle, Position } from '@xyflow/react';

import styles from './Handle.module.css';

interface Props {
  id?: string;
  position?: Position;
}

function OutputHandle(props: Props) {
  const { id = 'sr', position = Position.Right } = props;

  return (
    <Handle
      id={id}
      className={styles.output}
      type="source"
      position={position}
    />
  );
}

export default OutputHandle;
