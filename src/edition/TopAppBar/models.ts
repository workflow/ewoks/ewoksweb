export type Status = 'idle' | 'success' | 'error';

export type EngineDropdownOption = 'default' | 'dask' | 'pypushflow';
