import { IconButton, Tooltip } from '@mui/material';
import { useKeyboardEvent } from '@react-hookz/web';

import GraphFormDialog from '../../general/forms/GraphFormDialog';
import { useSaveWorkflow } from '../../general/hooks';
import tooltipText from '../../general/TooltipText';
import { useWorkflowHasChanges } from '../../store/graph-hooks';
import SuspenseBoundary from '../../suspense/SuspenseBoundary';
import SaveStatusIcon from './SaveStatusIcon';
import styles from './SaveToServerButton.module.css';

// DOC: Save to server button with its spinner
export default function SaveToServerButton() {
  const workflowHasChanges = useWorkflowHasChanges();

  const { isDialogOpen, setDialogOpen, status, setStatus, handleSave } =
    useSaveWorkflow();

  useKeyboardEvent(
    (e) => (e.ctrlKey || e.metaKey) && e.key === 's',
    (e) => {
      e.preventDefault();
      handleSave();
    },
    [],
  );

  const btnLabel = `Save workflow to server: ${
    workflowHasChanges ? ' changes pending' : ' no changes'
  }`;

  return (
    <>
      <SuspenseBoundary>
        <GraphFormDialog
          isOpen={isDialogOpen}
          onClose={() => setDialogOpen(false)}
        />
      </SuspenseBoundary>
      <Tooltip
        title={tooltipText('Save to server (Ctrl+S)')}
        enterDelay={500}
        arrow
      >
        <IconButton
          className={styles.saveButton}
          onClick={() => {
            void handleSave();
          }}
          aria-label={btnLabel}
          color="inherit"
          size="large"
        >
          <SaveStatusIcon status={status} setStatus={setStatus}>
            {workflowHasChanges && (
              <div className={styles.saveRedDot} data-testId="saveRedDot" />
            )}
          </SaveStatusIcon>
        </IconButton>
      </Tooltip>
    </>
  );
}
