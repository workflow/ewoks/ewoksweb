import { Paper, Tooltip } from '@mui/material';
import { useState } from 'react';

import { useIcons } from '../../../../api/icons';
import SuspenseBoundary from '../../../../suspense/SuspenseBoundary';
import DeleteIconButton from './DeleteIconButton';
import styles from './IconsDrawer.module.css';

function IconList() {
  const icons = useIcons();

  const [selectedIcon, setSelectedIcon] = useState<string>();

  return (
    <Paper className={styles.iconList}>
      {icons.map((icon) => (
        <div
          key={icon.name}
          className={styles.icon}
          data-selected={icon.name === selectedIcon || undefined}
        >
          <button
            className={styles.iconButton}
            onClick={() => setSelectedIcon(icon.name)}
            type="button"
            aria-label={icon.name}
          >
            <Tooltip title={icon.name} arrow placement="left">
              <img
                className={styles.iconImg}
                src={icon.data_url}
                alt={icon.name}
              />
            </Tooltip>
          </button>
          {icon.name === selectedIcon && (
            <div>
              <span className={styles.iconName}>{icon.name}</span>
              <SuspenseBoundary>
                <DeleteIconButton iconName={icon.name} />
              </SuspenseBoundary>
            </div>
          )}
        </div>
      ))}
    </Paper>
  );
}

export default IconList;
