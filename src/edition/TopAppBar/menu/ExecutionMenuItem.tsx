import SendIcon from '@mui/icons-material/Send';
import { useKeyboardEvent } from '@react-hookz/web';
import { useState } from 'react';

import useWorkflowStore from '../../../store/useWorkflowStore';
import ExecutionDialog from '../execution/ExecutionDialog';
import ActionMenuItem from './ActionMenuItem';

function ExecutionMenuItem() {
  const [open, setOpen] = useState(false);
  const { workflowInfo } = useWorkflowStore.getState();

  useKeyboardEvent(
    (e) =>
      (e.ctrlKey || e.metaKey) && e.shiftKey && e.key.toLowerCase() === 'x',
    (e) => {
      e.preventDefault();
      setOpen(true);
    },
    [],
  );

  return (
    <>
      <ExecutionDialog open={open} onClose={() => setOpen(false)} />
      <ActionMenuItem
        icon={SendIcon}
        label="Execute workflow"
        onClick={() => setOpen(true)}
        keyShortcut="Ctrl+Shift+X"
        disabled={!workflowInfo.id}
      />
    </>
  );
}

export default ExecutionMenuItem;
