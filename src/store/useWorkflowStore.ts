import type { ReactFlowInstance } from '@xyflow/react';
import { merge } from 'lodash';
import { create } from 'zustand';

import type { GraphDetails, Workflow } from '../ewoksTypes';
import type { NodeWithData, Task } from '../types';
import { WorkflowSource } from '../types';
import { getSubgraphs } from '../utils';
import { convertEwoksWorkflowToRFNodes } from '../utils/convertEwoksWorkflowToRFNodes';
import layoutNewGraph from '../utils/layoutNewGraph';
import { toRFEwoksLinks } from '../utils/toRFEwoksLinks';
import { validateWorkflow } from './storeUtils/validateWorkflow';
import useEdgeDataStore from './useEdgeDataStore';
import useNodeDataStore from './useNodeDataStore';
import useSnackbarStore from './useSnackbarStore';

const EMPTY_INFO: GraphDetails = {
  id: '',
};

const EMPTY_GRAPH: Workflow = {
  graph: {
    id: '',
    label: '',
    input_nodes: [],
    output_nodes: [],
    uiProps: {},
  },
  nodes: [],
  links: [],
};

export interface State {
  workflowInfo: GraphDetails;
  workflowSource: WorkflowSource;
  mergeWorkflowInfo: (displayedWorkflowInfo: Partial<GraphDetails>) => void;
  loadWorkflow: (
    ewoksWorkflow: Workflow,
    rfInstance: ReactFlowInstance,
    tasks: Task[],
    source: WorkflowSource,
  ) => Promise<void>;
  resetWorkflow: (
    rfInstance: ReactFlowInstance,
    tasks: Task[],
  ) => Promise<void>;
}

const useWorkflowStore = create<State>((set, get) => ({
  workflowInfo: EMPTY_INFO,
  workflowSource: WorkflowSource.Empty,

  mergeWorkflowInfo: (newWorkflowInfo) => {
    set((state) => {
      return {
        ...state,
        workflowInfo: merge({}, state.workflowInfo, newWorkflowInfo),
      };
    });
  },

  loadWorkflow: async (
    ewoksWorkflow,
    rfInstance,
    tasks,
    source,
  ): Promise<void> => {
    const { showErrorMsg } = useSnackbarStore.getState();

    const validWorkflow = validateWorkflow(ewoksWorkflow);

    if (!validWorkflow.valid) {
      showErrorMsg(
        `Error in workflow JSON description: ${
          validWorkflow.invalidReason || ''
        }`,
      );
      return;
    }

    const { graph, nodes = [], links = [] } = ewoksWorkflow;
    // 1. Initialize the canvas while working on the new graph
    set({
      workflowInfo: EMPTY_INFO,
    });

    // 2. Get node-subgraphs for the graph
    const subgraphs = await getSubgraphs(nodes);

    // 3. Calculate the new graph given the subgraphs
    let rfNodes = convertEwoksWorkflowToRFNodes(graph, nodes, subgraphs, tasks);

    const notes: NodeWithData[] =
      graph.uiProps?.notes?.map((note) => {
        return {
          data: {
            ewoks_props: { label: note.label },
            task_props: { task_type: 'note', task_identifier: note.id },
            ui_props: {
              borderColor: note.borderColor,
            },
            comment: note.comment,
          },
          id: note.id,
          type: 'note',
          position: note.position,
          width: note.width,
          height: note.height,
        };
      }) || [];

    rfNodes = [...rfNodes, ...notes];

    const rfLinks = toRFEwoksLinks({ graph, nodes, links }, subgraphs, tasks);

    useNodeDataStore.getState().setDataFromNodes(rfNodes);
    useEdgeDataStore.getState().setDataFromEdges(rfLinks);

    set({
      workflowSource: source,
      workflowInfo: graph,
    });

    const nodesWithoutData = rfNodes.map((node) => {
      return { ...node, data: {} };
    });

    const edgesWithoutData = rfLinks.map((edge) => {
      return { ...edge, data: {} };
    });

    if (!rfNodes.some((nod) => nod.position.x !== 100)) {
      rfInstance.setNodes(
        await layoutNewGraph(nodesWithoutData, edgesWithoutData),
      );
      rfInstance.setEdges(rfLinks);
    } else {
      rfInstance.setNodes(nodesWithoutData);
      rfInstance.setEdges(edgesWithoutData);
    }
  },
  resetWorkflow: async (rfInstance, tasks) => {
    return get().loadWorkflow(
      EMPTY_GRAPH,
      rfInstance,
      tasks,
      WorkflowSource.Empty,
    );
  },
}));

export default useWorkflowStore;
