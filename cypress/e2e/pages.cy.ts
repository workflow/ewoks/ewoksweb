beforeEach(() => {
  cy.visit('http://localhost:3000');
});

it('should land on the edit page', () => {
  cy.location().should((loc) => {
    expect(loc.pathname).to.eq('/edit');
  });
  cy.findByRole('link', { name: 'Edit' }).should(($link) => {
    const classes = $link.attr('class');
    expect(classes).to.include('active');
  });
  cy.get('.react-flow').should('be.visible');
});

it('should switch to monitor page', () => {
  cy.findByRole('link', { name: 'Monitor' }).click();
  cy.location().should((loc) => {
    expect(loc.pathname).to.eq('/monitor');
  });
  cy.findByRole('link', { name: 'Monitor' }).should(($link) => {
    const classes = $link.attr('class');
    expect(classes).to.include('active');
  });
});

it('should restore an opened workflow when switching pages', () => {
  cy.loadGraph('tutorial_Graph');
  cy.hasNavBarLabel('tutorial_Graph');
  cy.hasVisibleNodes(16);
  cy.hasVisibleEdges(12);

  cy.findByRole('link', { name: 'Monitor' }).click();
  cy.waitForStableDOM();

  // The monitor page has another 'Edit' link if no workflow is executed.
  cy.findByRole('navigation').within(() =>
    cy.findByRole('link', { name: 'Edit' }).click(),
  );
  cy.waitForStableDOM();
  cy.hasNavBarLabel('tutorial_Graph');

  cy.hasVisibleNodes(16);
  cy.hasVisibleEdges(12);
});
