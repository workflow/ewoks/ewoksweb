# CHANGELOG.md

Release notes can be found on the
[Releases page](https://gitlab.esrf.fr/workflow/ewoks/ewoksweb/-/releases) of
the ewoksweb repository.
